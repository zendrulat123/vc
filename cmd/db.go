/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/cobra"
	"gitlab.com/zendrulat123/vcmd/dbconn"
)

// dbCmd represents the db command
var dbCmd = &cobra.Command{
	Use:   "db",
	Short: "A brief description of your command",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("db called")
		table, _ := cmd.Flags().GetString("table")
		if table == "" {
			fmt.Println("you didnt enter a table")
		}
		fmt.Println("Hallo " + table)

		
		dber(table)

	},
}

func init() {
	rootCmd.AddCommand(dbCmd)
	dbCmd.Flags().StringP("table", "t", "", "Set your table")
	dbCmd.Flags().StringP("field1", "1", "", "Set your field1")
	dbCmd.Flags().StringP("field2", "2", "", "Set your field2")
	dbCmd.Flags().StringP("text", "e", "", "Set your text")

}

func dber(table struct{}) {
	db, err := dbconn.GetConn()
	if err != nil {
		fmt.Println("db connect not good", err)
	}

	db.AutoMigrate(&table)

	user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}

	result := db.Create(&user) // pass pointer of data to Create
	fmt.Println(user.ID, result.Error, result.RowsAffected)
	defer db.Close()
	// _, table_check := db.Query("select * from " + table + ";")

	// if table_check == nil {

	// } else {
	// 	fmt.Println("table not there")
	// 	fmt.Println("CREATE TABLE IF NOT EXISTS " + table + "  (id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY, " + field1 + "  " + text + ")")
	// 	//TEXT NOT NULL https://stackoverflow.com/questions/61789889/how-to-create-a-mysql-database-table-in-go-and-perform-crud-operations
	// 	_, err := db.Exec("CREATE TABLE IF NOT EXISTS " + table + "  (id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY, " + field1 + "  " + field2 + " " + text + ")")
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// 	defer db.Close()
	// }

}
