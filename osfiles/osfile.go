package main

import (
	"fmt"
	"log"
	"os"
)

func Createfile(t string) {
	f, err := os.Create(t)
	if err != nil {
		log.Fatal(err)
	}
	l, err := f.WriteString("Hello World")
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}
	fmt.Println(l, "bytes written successfully")
	err = f.Close()
	if err != nil {
		log.Fatal(err)
	}
}
